BASE=clef2015-clickmodels

all: pdf bibtex

pdf:
	pdflatex $(BASE).tex

bibtex:
	-bibtex $(BASE)

view:
	open $(BASE).pdf

