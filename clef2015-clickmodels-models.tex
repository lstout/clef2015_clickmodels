%!TEX root = clef2015-clickmodels.tex

\section{Click Models}
\label{sec:methodology}
%	\vspace*{-.5\baselineskip}
In this section, we give an overview of all major click models for web search,
which we will then use in our comparative study

\begin{table}[t]
	\centering
	\caption{Notation used in the paper.}
	\label{table:notations}
	\begin{tabular}{c l @{\hspace{-2px}} c l}
		\toprule
		Symbol & Description & Symbol & Description \\
		\midrule
		$u$	& A document & $E$ & A random variable for document examination \\
		$q$	& A query & $R$ & A random variable for document relevance \\
		$s$ & A search query session & $C$ & A random variable for a click on a document \\
		$j$ & A document rank & $\epsilon$ & The examination parameter \\
		$c$ & A click on a document & $r$ & The relevance parameter \\
		$\mathcal{S}$ & A set of sessions \\
		\bottomrule
	\end{tabular}
\end{table}

\paragraph{Click-Through Rate Models}
Three simple click models, all based on click-through rates, predict click probabilities
by counting the ratio of clicks to the total number of impressions. In the simplest case
of Global CTR (GCTR) this ratio is computed globally for all documents,
while in Rank CTR (RCTR) it is computed separately for each rank $j$
and in Document CTR (DCTR) for each document-query pair $uq$:
\begin{align}
\textstyle
    P_{\mathit{GCTR}}(C_{u}=1) =&\textstyle r = \frac{1}{\sum_{s \in \mathcal{S}}|s|} \sum_{s \in \mathcal{S}} \sum_{u \in s} c_{uq} \label{eq:gctr_rel} \\
    P_{\mathit{RCTR}}(C_{u_j}=1) =&\textstyle r_j = \frac{1}{|\mathcal{S}|} \sum_{s \in \mathcal{S}} c_j \label{eq:rctr_rel} \\
    P_{\mathit{DCTR}}(C_{u}=1) =&\textstyle r_{uq} = \frac{1}{|\mathcal{S}_{uq}|} \sum_{s \in \mathcal{S}_{uq}} c_{uq} \label{eq:ctr_rel}\text{, where}, \mathcal{S}_{uq} = \{ s_q : u \in s_q \}
\end{align}

\paragraph{Position-Based Model}
This model builds upon the CTR models and unites DCTR with RCTR\@.
It adds a separate notion of examination probability ($E$) which is subject
to \emph{position bias} where documents with smaller rank
are examined more often; the document can only be clicked if it was examined and is relevant:
\begin{equation}
    C_{uq}=1 \Leftrightarrow \left(E_{j_u}=1 \text{\ and } R_{uq}=1 \label{eq:exam_hypothesis}\right)
\end{equation}
The examination probability $\epsilon_j = P(E_{j_u}=1)$ depends on the rank $j$, while
the relevance $r_{uq} = P(R_{uq}=1)$ depends on the document-query pair.
Inference of this model is done using the Expectation Maximization algorithm (EM).

\paragraph{Cascade Model}
The Cascade Model~\cite[CM]{Craswell2008:An-experimental}
is another extension to the CTR models.
The model introduces the \textit{cascade hypothesis}, whereby a user examines
a search result page (SERP) from top to bottom, deciding whether to click each result
before moving to the next one; users stop examining a SERP after first click.
Inference of the parameters of CM is done using Maximum Likelihood Estimation (MLE).
The click probability is defined using the examination~\eqref{eq:exam_hypothesis}
and the cascade assumptions:
\begin{align}
    P(E_1 = 1) =&\ 1 \\
    P(E_j = 1 \mid E_{j-1} = e, C_{j-1} = c) =&\ e \cdot (1 - c),
    \label{eq:CM_continuation}
\end{align}
where $e$ and $c$ are $0$ or $1$, and the only parameters of the models are
$r_{uq} = P(R_{uq}=1)$.
The fact that users abandon a search session after the first click implies that the model
does not provide a complete picture of how multiple clicks arise in a query session
and how to estimate document relevance from such data.

\paragraph{User Browsing Model}
\citet{Dupret2008:A-user} propose a click model called the User Browsing Model (UBM). The main difference between UBM and other models is that UBM takes into account
the distance from the current document $u_j$ to the last clicked document $u_{j'}$
for determining the probability that the user continues browsing:
\begin{equation}
P(E_{j_u} = 1 \mid C_{u_{j'}}=1, C_{u_{j'+1}}=0, \dots, C_{u_{j-1}q}=0) = \gamma_{jj'}.
\end{equation}
%
%A graphical representation of the model is presented in Figure~\ref{fig:ubm_gm}.

\paragraph{Dependent Click Model}
The Dependent Click Model (DCM) by~\citet{Guo2009:Efficient}
is an extension of the cascade model that is meant to handle sessions with multiple clicks.
This model assumes that after a user clicked a document, they may still continue
to examine other documents. In other words, \eqref{eq:CM_continuation} is replaced by
\begin{equation}
    P(E_j = 1 \mid E_{j-1} = e, C_{j-1} = c) = e \cdot (1 - c + \lambda_j c),
    \label{eq:DCM_continuation}
\end{equation}
where $\lambda_j$ is the continuation parameter, which depends on the rank $j$ of
a document.

\paragraph{Click Chain Model}
\citet{Guo2009:Click} further extend the idea of DCM into the Click Chain Model (CCM).
The intuition behind CCM is that the chance that a user continues after a click depends
on the relevance of the previous document and that a user might abandon
the search after a while. This model can be formalized with \eqref{eq:exam_hypothesis}
and the following conditional probabilities:
\begin{align}
    P(E_{j_u+1}=1 \mid E_{j_u}=1,C_{uq}=0) =&\ \tau_1 \\
    P(E_{j_u+1}=1 \mid E_{j_u}=1,C_{uq}=1) =&\ \tau_2 (1 - r_{uq}) + \tau_3 r_{uq}.
\end{align}

\paragraph{Dynamic Bayesian Network Model}
The Dynamic Bayesian Network model~\citep[DBN]{Chapelle2009:A-dynamic}
takes a different approach in extending the cascade model.
Unlike CCM, DBN assumes that the user's perseverance after a click
depends not on the relevance $r_{uq}$, but on a different parameter $s_{uq}$ called
satisfaction parameter. While $r$ is mostly defined by the snippet on the SERP,
the satisfaction parameter $s$ depends on the actual document content available after a click.
The DBN model is defined by \eqref{eq:exam_hypothesis} and the following formulas:
\begin{align}
    P(E_{j_u+1}=1 \mid E_{j_u}=1,C_{uq}=0) =&\ \gamma \\
    P(E_{j_u+1}=1 \mid E_{j_u}=1,C_{uq}=1) =&\ \gamma (1 - s_{uq}),
\end{align}
where $\gamma$ is a continuation probability after a non-satisfactory document
(either no click, or click, but no satisfaction).

In general, the inference should be done using the EM algorithm.
However, if $\gamma$ is set to $1$, the model allows easy MLE inference.
We refer to this special case as the Simplified DBN model (SDBN).
