%!TEX root = clef2015-clickmodels.tex

\section{Results}
%\vspace*{-.5\baselineskip}
In this section we present the results of our experiments. For every evaluation measure we report the influence of the query frequency and click entropy. Table~\ref{table:results} contains the evaluation outcomes for every model when trained on the entire dataset.

\paragraph{Log-likelihood}
Figure~\ref{fig:ll} shows the results of
the log-likelihood experiments; shorter bars indicate better results.
The cascade model (CM) cannot handle multiple clicks in one session
and gives zero probability to all clicks below the first one.
For such sessions its log-likelihood is $\log 0 = - \infty$
and so the total log-likelihood of CM is~$-\infty$.

When evaluated on the whole test set, UBM shows the best log-likelihood,
followed by DBN, PBM and CCM\@.
Note that the simplified DBN model (SDBN)
has lower log-likelihood values compared to its standard counterpart (DBN).
The simple CTR-based models show the lowest log-likelihood.
This confirms that complex click models explain and approximate user behavior better than simply counting clicks.

Figure~\ref{fig:ll}~(left) shows the log-likelihood of click models for different query frequencies.
In general, the higher the query frequency (more training data available) the better the performance of click models.
When comparing complex click models, there is variation
in their relative performance based on the query frequency,
but UBM consistently has the highest log-likelihood.
SDBN and DCM have considerably lower log-likelihood than
the similar models DBN and CCM (apart from the ``20+'' bin).
In contrast, the log-likelihood of the CTR-based models varies considerably across query frequencies.
On the ``2'' and ``3--5'' bins, GCTR outperforms SDBN and DCM,
while RCTR is the second best model overall (after UBM).
The DCTR model has the lowest log-likelihood for all query frequencies, but ``20+''.
There, it outperforms SDBN, DCM and CCM and comes close to PBM\@.
These results show two interesting facts.
On the one hand, the log-likelihood of complex click models is more stable across different query frequencies
than that of the CTR-based models.
On the other hand, for each query frequency bin
there is a CTR-based model that has log-likelihood scores comparable to complex models
(RCTR for ``2--19'' and DCTR for ``20+'').

Figure~\ref{fig:ll}~(right) shows the log-likelihood of click models for queries with different click entropy.
In general, the lower the click entropy the easier it is to approximate clicks
and, hence, the better the performance of click models.
The relative log-likelihood of different click models for different values of click entropy is similar to that for different query frequencies:
UBM is followed in different orders by DBN, PBM and CCM;
SDBN and DCM have lower log-likelihood than the above;
the log-likelihood of the CTR-based models varies across bins
(RCTR is better than SDBN and DCM on $(1, 2]$, DCTR is comparable to PBM and CCM
on $(2, \infty)$).
As a future work, we plan to investigate the relation
between query frequency and click entropy.

\begin{figure}[t]
    \centering
    \includegraphics[width=\textwidth]{figures/LL.pdf}
    \caption{Log-likelihood of click models, grouped by query frequency (left) and click entropy (right).}
    \label{fig:ll}
    \vspace*{-1.5\baselineskip}
\end{figure}


\paragraph{Perplexity}
Figure~\ref{fig:perp} shows the perplexity of the click models; the lower the better.
When evaluated on all test sessions,
most of the complex click models (apart from CM and CCM) have comparable perplexity,
with DBN and SDBN having the lowest one, but not significantly so.
The CTR-based models have higher perplexity than the complex models,
which again confirms the usefulness of existing click models for web search.

The trends for different query frequencies (Figure~\ref{fig:perp},~left)
are similar to those for log-likelihood (Figure~\ref{fig:ll},~left):
the variation of perplexity of complex click models is not large
(but there are different winners on different bins),
while the perplexity of the CTR-based models varies considerably
(RCTR has the lowest perplexity overall on ``2'' and ``3--5'',
DCTR is comparable to other models on ``20+'').
The trends for different values of click entropy are similar
(see Figure~\ref{fig:perp},~right).
CM performs poorly in all query classes apart from the $[0, 1]$ entropy bin,
which is related to the fact that CM is tuned to explain sessions with one click.

\if0
\todo{Do we want this discussion? The figure not readable anyway.}
\todo{Update the text (TCM??) and the picture or remove them.}
Figure~\ref{fig:perp_rank} shows perplexity at different ranks. As a document has a lower rank the model's perplexity also gets lower.
However, the rate with which it gets lower differs per model.
TCM and PBM have a high perplexity for higher ranks, the documents that are more often examined by the users, whereas for low ranks they are the top performers in terms of perplexity. Hence, for position-based click models, of which TCM is one,
the examination parameters are more accurate than the examination parameter
inferred by other models.
\fi

\begin{figure}[t]
    \centering
    \includegraphics[width=\textwidth]{figures/Perp.pdf}
    \caption{Perplexity of click models, grouped by query frequency (left) and click entropy (right).}
    \label{fig:perp}
    \vspace*{-\baselineskip}
\end{figure}

\if0
\begin{wrapfigure}[12]{r}[0pt]{.5\textwidth}
    \centering
%    \includegraphics[width=.5\textwidth,clip=true,trim=15mm 5mm 10mm 0mm]{figures/Perp_rank.pdf}
    \includegraphics[width=.5\textwidth]{figures/Perp_rank.pdf}
    \caption{Perplexity of click models at different document rank position.}
    \label{fig:perp_rank}
    \vspace*{-\baselineskip}
\end{wrapfigure}
\fi


\paragraph{CTR prediction}
%
%
%
\if0
\todo{The results look  very strange here.
Theoretically, this evaluation should somewhat correlate with perplexity@1.
But what we see is that the EM-based models predict clicks at the first position very inaccurately.
Why could that be?
RCTR and DCTR should also work pretty well here (especially on ``20+''),
but this does not happen. I suspect there is a bug somewhere.}
\fi
%
%
%
Figure~\ref{fig:ctr} shows the impact of query frequency
and click entropy on the CTR prediction task.
Here, the simple models, RCTR and CM, outperform some of the more complex ones.
This is because the intuition of these models is exactly what this task has set out to measure.
The average rank of the documents in the training data set is $2.43$, i.e.,
they were usually in some of the top positions.
As the RCTR and CM models both perform well on documents that are ranked high,
this high average rank influences the observed performance.
The top performers on this task are sDBN and DCM. It is not clear why there is such a notable gap in performance between DBN and sDBN on this task; it could be speculated that DBN relies more on the satisfactoriness parameters that are not used in this task. Both UBM and PBM have poor performance on this task, we hypothesize that they rely even more on the position dependent parameters and in this task the document under question was presented at a different position. 
%As we saw in Section~\ref{sec:ctr}, this task is designed to take away the position bias by only testing on sessions where the document appears in the first place. It is disappointing that the complex models perform so much worse. This may have to do with the fact that every time the models are trained on a very small sample of the entire dataset. A consequence of this is that the other parameters that are used by the complex models besides the relevance parameter, have little data to train on, which influences the relevance parameter. Because of this we propose to change the method for calculating the CTR prediction. By not training on one query at a time but on all queries at the same time the other parameters will train better and thus models that use EM-inference have a better chance to learn the actual relevance of a document.


\begin{figure}[t]
    \centering
    \includegraphics[width=\textwidth]{figures/CTR.pdf}
    \caption{Click-through rate prediction RMSE of click models, grouped by query frequency (left) and click entropy (right).}
    \label{fig:ctr}
%    \vspace*{-\baselineskip}
\end{figure}


\paragraph{Relevance prediction}
The results of the relevance prediction task can be seen in Figure~\ref{fig:rel_ce}.
The plot for different query frequencies could not be generated,
because the queries with judged results do not occur often in the dataset,
while the relevance prediction protocol only considers queries that occur at least ten times.

The relevance prediction performance of all click models is relatively low
(between $0.500$ and $0.581$).
The GCTR and RCTR models do not have a document-specific parameter
and, thus, cannot predict relevance.
So their AUC is equal to that of random prediction, i.e., $0.5$.
UBM and PBM have the highest AUC ($0.581$),
while other models are closer to random prediction (from $0.515$ for CM to $0.541$ for CCM).
These results show that existing click models still have a long way to go before
they can be used for approximating relevance labels produced by human annotators.

\begin{figure}[ht]
    \centering
    \includegraphics[width=\textwidth]{figures/Rel_Pred_AUC_ce.pdf}
    \vspace*{-.5\baselineskip}
    \caption{Relevance prediction of click models on click entropy}
    \label{fig:rel_ce}
%    \vspace*{-\baselineskip}
\end{figure}

\paragraph{Predicted relevance as a ranking feature}
Figure~\ref{fig:rank} shows the results of using the predicted relevance as a ranking feature.
The best model here is CCM, followed by the simple DCTR model.
This is not surprising as relevant documents attract more clicks and usually have higher CTRs.
Thus, ranking documents based on their CTR values only (as done by DCTR) results in high NDCG@5.
Notice, though, that predicting actual relevance labels of documents based on the documents' CTRs is still a difficult task (see the discussion above).

The GCTR and RCTR models do not have document-specific parameters
and, thus, cannot rank documents. Therefore, they have the lowest values of NDCG@5. They still have high values of NDCG because no reranking was done for documents with equal relevance estimates, hence the values of NDCG for GCTR and RCTR reflect the ranking quality of the original ranker.

\begin{figure}[t]
    \centering
    \includegraphics[width=\textwidth]{figures/Ranking_NDCG.pdf}
    \caption{Ranking performance (NDCG@5) of click models, grouped by query frequency (left) and click entropy (right).}
    \label{fig:rank}
    \vspace*{-\baselineskip}
\end{figure}


\paragraph{Computation time}
\if0
\todo{Describe the hardware you used.}
\fi
In Table~\ref{table:results} we see that, as expected, the models that use MLE inference are much faster than those with EM inference.
When using EM inference to calculate the parameters of a click model, one would ideally use some convergence criteria; we have chosen to do a fixed number of iterations (i.e., 50).
%These training times depend on the number of queries that the model is trained upon and the number of parameters a model has.
Notice that UBM is 5--6 times faster than DBN and CCM, even though they all use EM\@.
DBN and CCM use more complex update rules and this results in such a big difference in training time.

\begin{table}[t]
    \centering
    \small
    \caption{Performance of click models according to various measures: log-likelihood ($\mathcal{LL}$), perplexity, RMSE of the CTR prediction task, AUC of the relevance prediction task, Pearson correlation between annotated relevances and predicted relevances, ranking performance (NDCG@5), and computation time. The symbol \dubbelop\ denotes a significant difference at $p=0.01$ as measured by a two tailed t-test. }
    \label{table:results}
    \begin{tabular}{l@{~~~}c@{~~~}c@{~~~}c@{~~~}c@{~~~}c@{~~~}c@{~~~}r}
        \toprule
        Model    &    $\mathcal{LL}$    &    Perplexity    &    RMSE    &    AUC    &    Pearson Correlation    &    NDCG@5    &    Time (sec.)\\
        \midrule
        GCTR    &    -0.369    &    1.522    &    0.372    &    0.500    &    0.000    &    0.676    &    0.597 \\
        RCTR    &    -0.296    &    1.365    &    0.268    &    0.500    &    0.000    &    0.676    &    \textbf{0.589}\dubbelop \\
        DCTR    &    -0.300        &    1.359    &    0.261    &    0.535    &    0.054    &    0.743    &    3.255 \\
        PBM    &    -0.267    &    1.320    &    0.354    &    \textbf{0.581}\dubbelop        &    0.128        &    0.727    &    34.299 \\
        CM    &    $\infty$    &    1.355    &    0.239    &    0.515    &    0.024    &    0.728    &    4.872 \\
        UBM    &    \textbf{-0.249}\dubbelop    &    1.320    &    0.343    &    \textbf{0.581}\dubbelop    &    \textbf{0.130}\dubbelop    &    0.735    &    82.778 \\
        DCM    &    -0.292    &    1.322    &    \textbf{0.212}\dubbelop    &    0.516    &    0.035    &    0.733    &    5.965 \\
        CCM    &    -0.279    &    1.341    &    0.283    &    0.541    &    0.106    &    \textbf{0.748}    &    521.103 \\
        DBN    &    -0.259    &    \textbf{1.318}\dubbelop    &    0.286    &    0.517    &    0.089    &    0.719    &    457.694 \\
        SDBN    &    -0.290    &    \textbf{1.318}\dubbelop    &    \textbf{0.212}\dubbelop    &    0.529    &    0.076    &    0.721    &    3.916 \\
        \bottomrule \\
    \end{tabular}
        \vspace*{-\baselineskip}
\end{table}


\paragraph{Overall results}
We summarize our experimental results in Table~\ref{table:results}.
There is no perfect click model that outperforms all other models on every evaluation metric.
For example, UBM is the best in term of log-likelihood and relevance prediction,
while DBN is the best in terms of perplexity and CTR prediction.
Even simple CTR-based models have relatively high performance according to some metrics (e.g., DCTR according to NDCG@5).
