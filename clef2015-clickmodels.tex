%
\documentclass{llncs} % For LaTeX2e

\usepackage{algorithmicx}
\usepackage[ruled,vlined]{algorithm2e}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{csvsimple}
\usepackage{etex}
%\usepackage[margin=1in]{geometry}
\usepackage{hyperref}
\usepackage{mathtools}
\usepackage[square,comma,numbers,sort&compress,sectionbib]{natbib}
\usepackage{pgfplots}
\usepackage[justification=centering]{subcaption}
\usepackage{siunitx}
\usepackage{times}
\usepackage{url}
\usepackage{setspace}
\usepackage{wrapfig}

% Should be the last one.
\usepackage{hyperref}

\usetikzlibrary{bayesnet}
\pgfplotsset{compat=newest}

% Use \num{} to format numbers American way, i.e. with comma as a thousand separator.
\sisetup{group-separator = {,}}
\sisetup{group-minimum-digits = 4}
%\newcommand{\num}{${\vartriangle}$}

\newcommand\todo[1]{\textcolor{red}{\textbf{TODO:} #1}}
\newcommand\ilya[1]{\textcolor{blue}{\textbf{Ilya:} #1}}
\newcommand\artem[1]{\textcolor{green}{\textbf{Artem:} #1}}
\newcommand\aleksandr[1]{\textcolor{cyan}{#1}}
\newcommand\mdr[1]{\textcolor{orange}{\textbf{MdR:} #1}}
\newcommand\luka[1]{\textcolor{purple}{\textbf{Luka:} #1}}
\newcommand\finde[1]{\textcolor{orange}{\textbf{Finde:} #1}}

\newenvironment{loglisting}[1][htb]
{\renewcommand{\algorithmcfname}{Log listing}% Update algorithm name
  \begin{algorithm}[#1]%
}{\end{algorithm}}

\makeatletter
\algrenewcommand\ALG@beginalgorithmic{\ttfamily}
\makeatother

\urldef{\mailsa}\path|lukastout@gmail.com,|
\urldef{\mailsb}\path|finde.findexumara@student.uva.nl,|
\urldef{\mailsc}\path|{a.grotov, a.chuklin, i.markov}@uva.nl,|
\urldef{\mailsd}\path|derijke@uva.nl|

\newcommand{\keywords}[1]{\par\addvspace\baselineskip
\noindent\keywordname\enspace\ignorespaces#1}

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}
\newcommand{\comment}[1]{\textit{\color{red}{#1}}}

\newcommand{\squishlist}{
 \begin{list}{$\bullet$}
  { \setlength{\itemsep}{0pt}
     \setlength{\parsep}{3pt}
     \setlength{\topsep}{3pt}
     \setlength{\partopsep}{0pt}
     \setlength{\leftmargin}{1.5em}
     \setlength{\labelwidth}{1em}
     \setlength{\labelsep}{0.5em} } }

\newcommand{\squishlisttwo}{
 \begin{list}{$\bullet$}
  { \setlength{\itemsep}{0pt}
     \setlength{\parsep}{0pt}
    \setlength{\topsep}{0pt}
    \setlength{\partopsep}{0pt}
    \setlength{\leftmargin}{2em}
    \setlength{\labelwidth}{1.5em}
    \setlength{\labelsep}{0.5em} } }

\newcommand{\squishend}{
  \end{list}  }
  
\newcommand{\enkelop}{$^{\vartriangle}$}
\newcommand{\dubbelop}{$^{\blacktriangle}$}
\newcommand{\enkelneer}{$^{\triangledown}$}
\newcommand{\dubbelneer}{$^{\blacktriangledown}$}

\renewcommand{\paragraph}[1]{\smallskip\noindent\textbf{#1.~}}

\parskip0pt

\begin{document}

\mainmatter

\title{A Comparative Study of Click Models for Web Search}

\author{Artem Grotov \and Aleksandr Chuklin\thanks{Currently at Google Switzerland; email: chuklin@google.com} \and Ilya Markov \\ Luka Stout \and Finde Xumara \and Maarten de Rijke}

\institute{University of Amsterdam, Amsterdam, The Netherlands\\
\mailsc \;\; \mailsa \;\; \mailsb \;\; \mailsd}

\authorrunning{A Comparative Study of Click Models for Web Search}

\maketitle

\begin{abstract}
    Click models have become an essential tool for understanding user behavior on
    a search engine result page, running simulated experiments and predicting relevance.
    Dozens of click models have been proposed, all aiming
    to tackle problems stemming from the complexity of user behavior or
    of contemporary result pages. Many models
    have been evaluated using proprietary data, hence the results are hard to reproduce.
    The choice of baseline models is not always motivated and the fairness of
    such comparisons may be questioned.
%
    In this study, we perform a detailed analysis of all major click models for web search ranging from
    very simplistic to very complex. We employ a publicly available
    dataset, open-source software and a range of evaluation techniques,
    which makes our results both representative and reproducible.
%    We show that in some cases simple click models outperform state-of-the art models.
    We also analyze the query space to show what type of queries
    each model can handle best.
\end{abstract}

\input{clef2015-clickmodels-intro}
\input{clef2015-clickmodels-models}
\input{clef2015-clickmodels-metrics}
\input{clef2015-clickmodels-experiments}
\input{clef2015-clickmodels-results}
\input{clef2015-clickmodels-conclusions}

\begin{spacing}{1}
\bigskip\noindent\small
\textbf{Acknowledgements.}
This research was supported by
%
grant P2T1P2\_152269 of the Swiss National Science Foundation,
%
Amsterdam Data Science,
%
the Dutch national program COMMIT,
%
Elsevier,
%
the European Community's Seventh Framework Program\-me (FP7/2007-2013) under
grant agreement nr 312827 (VOX-Pol),
%
the ESF Research Network Program ELIAS,
%
the HPC Fund,
%
the Royal Dutch Academy of Sciences (KNAW) under the Elite Network Shifts project,
%
the Microsoft Research PhD program,
%
the Netherlands eScience Center under project number 027.012.105,
%
the Netherlands Institute for Sound and Vision,
%
the Netherlands Organisation for Scientific Research (NWO)
under pro\-ject nrs
%
727.\-011.\-005, % SEED
612.001.116, % ImFIRE
HOR-11-10, % HORIZON
640.\-006.\-013, %DADAISM
612.\-066.\-930, %Floor
CI-14-25, %MediaNow
SH-322-15, %Cartesius
%
and
%
the Yahoo! Faculty Research and Engagement Program.

All content represents the opinion of the authors which is not necessarily shared or endorsed by
their respective employers and/or sponsors.
\end{spacing}

\bibliographystyle{splncsnat}
{\small
\bibliography{clef2015-clickmodels}
}

%\newpage
%\appendix
%\input{appendix_tcm.tex}
%\input{appendix_ccm.tex}

\end{document}
