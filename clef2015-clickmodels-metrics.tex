%!TEX root = clef2015-clickmodels.tex

\section{Evaluation Measures}
\label{sec:evaluation}
\if0
To equally evaluate each click model's performance, we use evaluation metrics
that have been proposed in the papers accompanying the proposals of these click models.
The evaluation metrics used in this experiment are listed below.
\fi

Different studies use different metrics to evaluate click models~\cite{chuklin-click-2015}.
In this section we give an overview of these metrics.
We will then use all of them in our comparative study.

\paragraph{Log-likelihood}
Log-likelihood evaluates how well a model approximates observed data.
In our case, it shows how well a click model approximates clicks of actual users.
Given a model $M$ and a set of observed query sessions $\mathcal{S}$,
log-likelihood is defined as follows:
\begin{equation}
    \label{eq:loglikelihood}
    \mathcal{LL}(M) = \textstyle\sum_{s \in \mathcal{S}} \log P_M\left(C_1, \ldots, C_n\right),
\end{equation}
where $P_M$ is the probability of observing a particular sequence of clicks $C_1, \ldots, C_n$ according to the model $M$.

\paragraph{Perplexity}
Perplexity measures how surprised a model is to see a click at rank $r$ in a session $s$~\cite{Dupret2008:A-user}.
It is calculated for every rank individually:
\begin{equation}
\label{eq:perp_prob}
    p_r(M) = 2^{-\frac{1}{|\mathcal{S}|} \sum_{s \in \mathcal{S}} \left(
        c_r^{(s)} \log_2{q_r^{(s)}} +
        \left(1-c_r^{(s)}\right) \log_2{\left(1-q_r^{(s)}\right)}\right)},
\end{equation}
where $c_r^{(s)}$ is the actual click on the document at rank $r$ in the session $s$,
while $q_r^{(s)}$ is the probability of a user clicking the document at rank $r$ in the session $s$
as predicted by the model $M$, i.e., $q_r^{(s)} = P_M(C_r = 1)$.

The total perplexity of a model is defined as the average of perplexities over all positions.
Lower values of perplexity correspond to higher quality of a click model.

\paragraph{Click-trough rate prediction}
\label{sec:ctr}
Click-through rate (CTR) is a ratio of the cases when a particular document was clicked to the cases when it was shown.
\if0
The purpose of click-through rates is to measure the ratio of clicks to impressions of an document.
Generally the higher the CTR the higher chance of that document being clicked.
The click-through rate of a document $d$ is defined as:
\begin{align}
	CTR_d = \frac{1}{|S_d|} \sum_{s_d} c_{r_d}^{(s_d)}
\end{align}
where $S_d$ is the set of sessions where document $d$ appears.
\fi
In \cite{Chapelle2009:A-dynamic}, the following procedure was proposed to measure the quality of click models using CTRs:
\squishlisttwo
	\item Consider a document $u$ that appears both on the first position and on some other positions (in different query sessions).
	\item Hold out as a test set all the sessions in which $u$ appears on the first position.
	\item Train a click model $M$ on the remaining sessions.
	\item Use the model $M$ to predict clicks on the document $u$ on the held-out test set (predicted CTR).
	\item Compute the actual CTR of $u$ on the held-out test set.
	\item Compute the Root-Mean-Square-Error (RMSE) between the predicted and actual CTRs.
\squishend

\paragraph{Relevance prediction}
\if0
Relevance prediction was used to evaluate the performance of the DBN model \cite{Chapelle2009:A-dynamic}.
The accuracy of CTR prediction may not directly translate to relevance, especially when we were to evaluate the whole task instead of a single query.
In this case, the CTR of a particular document is highly dependent on the user-model assumptions.
For example if a user tends to ignore a document that is not fresh, the CTR will be low even if the document is relevant.
To measure relevance prediction we use a hand annotated set. For a group of query-document pairs this set contains a relevance. For these pairs we use the models to predict the relevance. We then use the Area Under the Curve (AUC) between the annotated relevances and the predicted relevances as an evaluation measure. We also calculate the Pearson correlation between the two.
\fi
It was noticed in~\cite{Chapelle2009:A-dynamic} that click models can approximate document relevance.
A straightforward way to evaluate this aspect is to compare document relevance as predicted by a model
to document relevance labels provided by human annotators.
We measure the agreement between the two using the Area Under the ROC Curve (AUC) and Pearson correlation.

\paragraph{Predicted relevance as a ranking feature}
The predicted relevance can also be used to rank documents~\cite{Chapelle2009:A-dynamic}.
The performance of such a ranker can be evaluated using any standard IR measure, such as MAP, DCG, etc.
In this study, we use NDCG@5~\cite{NDCG}.
To calculate NDCG@5 we only consider documents for which we have relevance labels.
%All these queries are then averaged to calculate the ranking performance of the click model.
The evaluation is performed as follows:
%
\squishlisttwo
	\item Retrieve all sessions that have complete editorial judgments.
	\item Sort sessions by session id
	\item The first 75\% are training sessions, the remainder are test sessions.
	\item Train the model on the training sessions and predict relevance for the test sessions.
	\item Sort the documents w.r.t the predicted relevance given by the model.
	\item Compute the NDCG@5.
	\item Average over all sessions.
\squishend
%
\paragraph{Computation time}
Historically, in machine learning a big problem in creating accurate models was the amount of data that was available. However, this is no longer the case, and now we are mostly restricted by the time it takes to learn a model based on a large amount of available data. This makes the ability to efficiently compute parameters an important feature of a successful model. Therefore, we also look at the time it takes to train a click model.
